/*************************************************************************
 Title	:   C++ file for PID controller
 Author:    Stefan lehmann
 File:	    pid.cpp
 Software:  AVR-GCC 4.3.3
 Hardware:  any AVR device
***************************************************************************/
#include <avr/io.h>
#include <limits.h>
#include "pid.h"

int16_t limit(int16_t val, int16_t min, int16_t max)
{
	if (val > max)
		return max;
	if (val < min)
		return min;		
	return val;		
}

int32_t limit(int32_t val, int32_t min, int32_t max)
{
	if (val > max)
		return max;
	if (val < min)
		return min;		
	return val;
}
	
PIDController::PIDController(int16_t kp, int16_t ki, int16_t kd)
{
	this->delta_sum = 0;
	this->last_delta = 0;	
	this->kp = kp;
	this->ki = ki;
	this->kd = kd;
	this->w = 0;
	this->x = 0;
	this->y = 0;
	this->antiwindup = true;	
}

void PIDController::process()
{
	/****************************
	 * Limits to avoid overflow *
	 ****************************/
	//Set limits to avoid overflow
	this->max_delta = this->kp ? INT_MAX / this->kp : INT_MAX;
	
	int32_t max_iterm = SCALE_I * FQ * (this->ki ? MAX_I_TERM / (int32_t) this->ki : MAX_I_TERM);	
	this->max_deltasum = max_iterm;
	this->min_deltasum = - max_iterm;
	
	//difference	between setpoint and value
	this->delta = this->w - this->x;	
	
	//proportional part
	//-----------------
	if (this->delta / SCALE_P > this->max_delta)
		this->p_part = MAX_Y;
	else if (this->delta / SCALE_P< -this->max_delta)
		this->p_part = MIN_Y;
	else
		this->p_part = this->delta / SCALE_P * this->kp;		
			
	//integral part	
	//-------------
	if (this->antiwindup)	
	{
		if ((this->delta > 0) && (this->y < MAX_Y) ||
			(this->delta < 0) && (this->y > MIN_Y))
				this->delta_sum += this->delta;
	}
	else
	{
		this->delta_sum += this->delta;
	}	
	this->delta_sum = limit(this->delta_sum,
							this->min_deltasum,
							this->max_deltasum);
	i_part = this->ki * this->delta_sum / FQ / SCALE_I;	
	
	//differential part
	//-----------------
	d_part = (this->delta - this->last_delta) * this->kd * FQ / SCALE_D; 
	this->last_delta = this->delta;
	this->y = limit(p_part + i_part + d_part, MIN_Y, MAX_Y);
}
